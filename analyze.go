package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/cliarg"
)

const (
	flagSeverityLevel = "severity-level"

	pathOutput     = "/tmp/flawfinder.csv"
	pathFlawfinder = "/usr/local/bin/flawfinder"

	flagSASTAllowedCLIOpts = "sast-scanner-allowed-cli-opts"
)

var (
	// allowedCliOpts is the set of CLI options that are allowed to pass to
	// the underlying security scanner.
	allowedCliOpts = []string{"-n", "--neverignore"}
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.IntFlag{
			Name:    flagSeverityLevel,
			Usage:   "Severity level",
			EnvVars: []string{"SAST_FLAWFINDER_LEVEL"},
			Value:   1,
		},
		&cli.StringFlag{
			Name:    flagSASTAllowedCLIOpts,
			Usage:   "See https://docs.gitlab.com/ee/user/application_security/sast/#security-scanner-configuration",
			EnvVars: []string{"SAST_SCANNER_ALLOWED_CLI_OPTS"},
		},
	}
}

// ReadCloser wraps a Reader and implements a Close methods that does nothing.
type ReadCloser struct{ io.Reader }

// Close is a fake implementation.
func (r ReadCloser) Close() error {
	return nil
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	level := strconv.Itoa(c.Int(flagSeverityLevel))

	args := buildArgs(level, c.String(flagSASTAllowedCLIOpts))

	cmd := exec.Command(pathFlawfinder, args...)
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	if err != nil {
		log.Errorf("%s\n%s", cmd.String(), output)

		// Exit code 15 means that flawfinder ran into character encoding issues.
		if cmd.ProcessState.ExitCode() == 15 {
			docURL := "https://docs.gitlab.com/ee/user/application_security/sast/#flawfinder-encoding-error"
			log.Info("It appears that you have run into an issue with character encoding.")
			log.Infof("Please visit %v for docs on approaches to fixing character encoding issues.", docURL)
		}

		return nil, err
	}

	log.Debugf("%s\n%s", cmd.String(), output)

	return ioutil.NopCloser(bytes.NewReader(output)), nil
}

func buildArgs(level string, allowedCLIOpts string) []string {
	defaultArgs := []string{"-m", level, "--csv"}
	userArgs := make([]string, 0)

	args, invalid := cliarg.Parse(allowedCLIOpts)
	if len(invalid) > 0 {
		log.Warnf("skipping following values as they are not represented under any flag: %s", invalid)
	}

	for _, arg := range args {
		if !isFlagAllowed(arg) {
			log.Warnf("skipping '%s' arg as it does not fall under allowed list of CLI args: %s", arg.Name, allowedCliOpts)
			continue
		}
		if arg.IsFlag {
			userArgs = append(userArgs, arg.Name)
		} else {
			userArgs = append(userArgs, arg.Name, arg.Value)
		}
	}

	builtArgs := append(defaultArgs, userArgs...)
	builtArgs = append(builtArgs, ".")
	return builtArgs
}

func isFlagAllowed(flag cliarg.Arg) bool {
	for _, opt := range allowedCliOpts {
		if flag.Name == opt {
			return true
		}
	}
	return false
}
